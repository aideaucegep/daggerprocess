import redis
from typing import Callable
import json
import jsons
from datetime import datetime
import time
from status import Status


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    WHITE = '\u001b[37m'


class Message:
    def __init__(self, _id, body, timestamp=None):
        self.id = _id
        self.body = body
        self.type = "Message"
        self.timestamp = timestamp or datetime.now()

    def __repr__(self):
        return json.dumps({"type": self.type, "id": self.id, "body": self.body, "timestamp": self.timestamp})

    def __str__(self):
        if self.type == "StatusMessage":
            return f"{self.timestamp} {self.type:<13} {self.id:<5} {bcolors.HEADER} {self.body} {bcolors.ENDC}"
        else:
            return f"{self.timestamp} {self.type:<13} {self.id:<5} {bcolors.WHITE} {self.body} {bcolors.ENDC}"


class StatusMessage(Message):
    def __init__(self, _id, status: Status, timestamp=None):
        self.id = _id
        self.type = "StatusMessage"
        if isinstance(status, str):
            self.body = status
        else:
            self.body = status.name
        self.timestamp = timestamp or datetime.now()


class MessagingManager():
    def __init__(self, conn: redis.Redis):
        self.r = conn

    def post(self, msg: Message):
        self.r.publish('daggerprocess-messaging', jsons.dumps(msg))


class MessagingCollector():
    def __init__(self, conn: redis.Redis):
        self.r = conn
        self.p = self.r.pubsub()
        self.p.subscribe('daggerprocess-messaging')

    def deserialize(self, msg: str):
        dmsg = json.loads(msg)
        t = dmsg['type']
        if globals()[t]:
            return globals()[dmsg['type']](dmsg['id'], dmsg['body'], dmsg['timestamp'])

    def collect(self):
        while True:
            time.sleep(0.001)
            message = self.p.get_message()
            if message:
                if message['type'] == "message":
                    yield self.deserialize(message['data'].decode())
            else:
                break


r = redis.Redis(host='localhost', port=6379, db=0)
Messaging = MessagingManager(r)
Collector = MessagingCollector(r)
