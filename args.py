import pystache
from args_interpreter import Interpreter
renderer = pystache.Renderer()
interpreter = Interpreter()


def parse(tmplt):
    return pystache.parse(tmplt)


def render(arg):
    return renderer.render(arg, interpreter)


def prepare(args):
    prepared_args = []
    for arg in args:
        if type(arg) is str and "{" in arg:
            prepared_args.append(render(parse(arg)))
        else:
            prepared_args.append(arg)
    return prepared_args
