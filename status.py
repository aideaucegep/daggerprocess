from enum import Enum
class Status(Enum):
    WAITING=1
    RUNNING=2
    DONE=3
    ERROR=4
    UPDATED=5
