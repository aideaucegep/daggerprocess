from helpers import dynamicImport

args = {
    "waitforfile.Process": ["//Mtldoc/secteurs/CVA Desk/Temp/{{tm1:yyyymmdd}}.testfile.csv", "{{today}}"],
    "waitfixtime.Process": [2]
}


async def test(process: str):
    if(process.endswith(".")):
        process = process + "Process"
    processClass = dynamicImport("processes." + process)
    task = processClass([], "test" + process)
    await task.run(args[process])
