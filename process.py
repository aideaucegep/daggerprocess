from multiprocessing import Process
import time
from status import Status



class DaggerProcess:

    def __init__(self):
        self.status = Status.WAITING

    @property
    def ready(self):
        return True

    def run(self):
        self.status = Status.RUNNING
        self.process = Process(target=self.execute) #args=self.internalArgs)
        self.process.start()

    def pulse(self):
        pass


