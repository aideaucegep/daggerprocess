import coloredlogs
import logging
logger = logging.getLogger("main")

coloredlogs.install(
    level='DEBUG', fmt="%(asctime)s %(hostname)-8s %(processName)-16s %(levelname)-9s %(message)s", logger=logger)


def argsstr(args):
    acc = []
    for arg in args:
        try:
            acc.append(str(arg))
        except:
            pass
    return " ".join(acc)


def info(*args):
    return
    logger.debug(argsstr(args))


def debug(*args):
    logger.debug(argsstr(args))


def critical(*args):
    logger.critical(argsstr(args))


def error(*args):
    logger.error(argsstr(args))
