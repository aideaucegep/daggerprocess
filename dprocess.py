from multiprocessing import Process, Queue
import time
from status import Status
import uuid
from enum import Enum
from taskmanager import TaskManager
from processcollector import ProcessPool
from messaging import Messaging, Message, StatusMessage
import asyncio
import args as Args


class ProcessHandler:
    def __init__(self, handle):
        self.type = type(handle)
        self.handle = handle

    def kill(self):
        if self.handle:
            if self.type == asyncio.Task and not self.handle.done():
                self.handle.cancel()
            elif self.type == Process and self.handle.is_alive():
                self.handle.terminate()


class ProcessType(Enum):
    NORMAL = 1
    ASYNC = 2
    SUBPROCESS = 3
    REMOTE = 4


class DaggerProcess:
    def __init__(self, dependencies=[], id: str = None):
        self.dependencies = dependencies
        self.status = Status.WAITING
        self.id = id or str(uuid.uuid4())
        self.process_handle = None

    def execute(self, *args):
        Messaging.post(StatusMessage(self.id, Status.DONE))
        pass

    def kill(self):
        if self.process_handle:
            self.process_handle.kill()

    async def async_execute(self, *args):
        Messaging.post(StatusMessage(self.id, Status.DONE))
        pass

    def subprocess_execute(self, *args):
        Messaging.post(StatusMessage(self.id, Status.DONE))
        pass

    def dependencies_ready(self):
        return TaskManager.tasks_done(self.dependencies)

    def ready(self):
        # May be overwriten
        return self.dependencies_ready()

    def prepare_args(self, args):
        return Args.prepare(args)

    async def run(self, args=()):
        if isinstance(self, NullTask):
            return True
        print("starting", self.id)
        self.args = self.prepare_args(args)
        if self.process_type == ProcessType.NORMAL:
            self.execute(*self.args)
        elif self.process_type == ProcessType.SUBPROCESS:
            subprocess = Process(
                target=self.subprocess_execute, args=self.args, name=self.id)
            ProcessPool.add(subprocess)
            self.process_handle = ProcessHandler(subprocess)
            subprocess.start()
        elif self.process_type == ProcessType.ASYNC:
            self.process_handle = ProcessHandler(
                asyncio.create_task(self.async_execute(*self.args)))

    def __str__(self):
        return str(self.id)


class NullTask(DaggerProcess):
    def __init__(self):
        self.status = Status.DONE
        self.dependencies = []
        self.id = id or str(uuid.uuid4())
        self.process_type = ProcessType.NORMAL
        self.process_handle = None
