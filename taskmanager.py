from status import Status
from messaging import Messaging, StatusMessage
import log
import time
from messaging import Collector, Message, StatusMessage


class TaskManagerClass():
    def __init__(self):
        self.tasks = set()

    def get(self, pid):
        return next((task for task in self.tasks if task.id == pid), None)

    def add(self, task):  # task: Node
        return self.tasks.add(task)

    def dependsOn(self, task):
        return (t for t in self.tasks if task.id in t.dependencies)

    def updateStatus(self, pid, status, announce=False):
        if announce:
            Messaging.post(StatusMessage(pid, status))
        task = self.get(pid)
        task.updateStatus(status) if task else None
    

    def processMessages(self):
        for msg in Collector.collect():
            if isinstance(msg, StatusMessage):
                self.updateStatus(msg.id, Status[msg.body])


    def resetDag(self):
        if pid == "root":
            for task in self.tasks:
                if task and task.task and task.parent:
                    log.critical("KILLING:", task)
                    task.task.kill()
                    self.updateStatus(task.id, Status.WAITING, announce=True)
            time.sleep(1)
            self.processMessages()
            time.sleep(1)
            return

    def resetSubTree(self, pid=None, task=None):
        if not pid and not task:
            return
        if not task and pid:
            task = self.get(pid)
        if task:
            log.critical("KILLING:", task)
            if task.task:
                task.task.kill()
            self.updateStatus(pid or task.id, Status.WAITING, announce=True)
            for child in task.children:
                self.resetSubTree(task=child)

            for dependant in self.dependsOn(task):
                self.resetSubTree(task=dependant)

    def tasks_done(self, ids: list):
        return all(task.status == Status.DONE for task in self.tasks if task.id in ids)

    @property
    def state(self):
        return [{"id": task.id, "status": task.status} for task in self.tasks]


TaskManager = TaskManagerClass()
