from datetime import datetime, timedelta
import csv

with open("//mtldoc/Secteurs/CVA Desk/Sungard/Pricing Interface/calendar.csv") as f:
    holidays = [datetime.strptime(line[0], "%Y-%m-%d") for line in csv.reader(f)]

def is_holiday(date:datetime):
    return datetime in holidays

def is_week_day(date:datetime):
    return date.weekday() < 5

def is_work_day(date:datetime):
    return is_week_day(date) and not is_holiday(date)

def tmx(date:datetime, days:int):
    d = date
    for i in range(days):
        d = tm1(d)
    return d
            
def tm1(date:datetime):
    d = date
    while True:
        d = d - timedelta(days=1)
        if is_work_day(d):
            return d

def tm2(date:datetime):
    return tmx(date, 2)

def tm3(date:datetime):
    return tmx(date, 3)

def t(date):
    return date

