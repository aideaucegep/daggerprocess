from node import Node

class Dag(Node):
    dag = None
    def __init__(self, body):
        body['id'] = getattr(body, 'id', "root")
        super(Dag, self).__init__(body, None)


if  __name__ == "__main__":
    import yaml
    with open("./mydag.yml", 'r') as yamlfile:
        data = yaml.load(yamlfile, Loader=yaml.FullLoader)
        dag = Dag(data["root"])
        a = dag.children[0]
    