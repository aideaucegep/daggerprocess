from timemanager import TimeManager

class Interpreter(object):
    def private_today(self):
        return TimeManager.today
        
    def private_tm1(self, fmt=None):
        if fmt == "yyyymmdd":
            return TimeManager.tm1.strftime("%Y%m%d")


    def __getattr__(self, escape_node):
        particles = escape_node.split(":")
        fct = particles[0]
        params = particles[1:]
        return self.__getattribute__("private_" + fct)(*params)