import importlib
from status import Status
from dprocess import DaggerProcess
import asyncio
import uuid
import jsons
from taskmanager import TaskManager
from dprocess import NullTask
from helpers import dynamicImport


class Node:
    def __init__(self, body, parent):
        self.loadBody(body)
        self.parent = parent
        self.children = []

        self.task = None
        if not self.id:
            raise Exception(f"Node {self._process} must have an id")
        if not self._process:
            self.task = NullTask()
            self.task.status = Status.DONE

        children_body = body.get('children', None)
        if children_body:
            for children in children_body:
                self.children.append(Node(children, self))

        TaskManager.add(self)

    def loadBody(self, body):
        self.id = body.get('id')
        self._process: str = body.get('process')
        self.args = body.get('args', None)
        self.dependencies = body.get('dependencies', [])
        self.task = None

    @property
    def status(self):
        if self.task:
            return self.task.status

    def updateStatus(self, status: Status):
        if self.task:
            self.task.status = status

    def importProcess(self):
        if(self._process.endswith(".")):
            self._process = self._process + "Process"
        processClass = dynamicImport("processes." + self._process)
        self.task = processClass(self.dependencies, self.id)

    def pulseChildren(self):
        for child in self.children:
            child.pulse()

    def pulse(self):
        if isinstance(self.task, NullTask):
            self.pulseChildren()
            return
        if self._process and (not self.task or isinstance(self.task, NullTask)):
            self.importProcess()
        if self.task:
            if self.task.ready() and self.task.status == Status.WAITING:
                self.task.status = Status.RUNNING
                asyncio.create_task(self.task.run(self.args))
                return False
            if self.status == Status.DONE:
                self.pulseChildren()
        else:
            self.pulseChildren()

    def toDictionary(self, child: bool = True):
        if child:
            return {"id": self.id,  "args": self.args, "process": self._process, "dependencies": self.dependencies, "children": [child.toDictionary() for child in self.children], "size": [1, 1]}
        else:
            return {"id": self.id,  "args": self.args, "process": self._process, "dependencies": self.dependencies, "children": [child.id for child in self.children], "size": [1, 1]}

    def toJson(self):
        return jsons.dump(self.toDictionary(False))
