import redis
from messaging import MessagingCollector
import time
import asyncio
import websockets
from multiprocessing import Process
import json
import log


USERS = set()


async def broadcast(msg):
    if USERS:  # asyncio.wait doesn't accept an empty list
        await asyncio.wait([user.send(msg) for user in USERS])


async def register(websocket):
    USERS.add(websocket)


async def unregister(websocket):
    USERS.remove(websocket)


async def run(websocket, path):
    # register(websocket) sends user_event() to websocket
    await register(websocket)
    log.info("REGISTER SOCKET CLIENT:", websocket)
    try:
        async for message in websocket:
            log.info("RECEIVING MESSAGE FROM CLIENT:", message)
    except Exception as e:
        log.error(e)
    finally:
        await unregister(websocket)


async def start_redis_bridge():
    r = redis.Redis(host='localhost', port=6379, db=0)
    collector = MessagingCollector(r)
    try:
        while True:
            for message in collector.collect():
                log.info("MESSAGE:", str(message))
                await broadcast(repr(message))
            await asyncio.sleep(0.1)
    except Exception as e:
        log.error(e)
    return await start_redis_bridge()


async def startAsync():
    websockets.serve(run, 'localhost', 5678)
    asyncio.create_task(start_redis_bridge())


def serve():
    start_server = websockets.serve(run, 'localhost', 5678)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_until_complete(start_redis_bridge())
    asyncio.get_event_loop().run_forever()
    log.error("ERROR: SOCKER SERVER CLOSED")


def spawn():
    p = Process(target=serve)
    p.start()
    return p


if __name__ == "__main__":
    serve()
