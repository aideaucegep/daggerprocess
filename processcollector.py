class processpool():
    def __init__(self):
        self.processes = []

    def add(self, proc):
        self.processes.append(proc)
    
    
    def killall(self):
        for proc in self.processes:
            proc.terminate()
        self.collect_garbage()
        
        
    def collect_garbage(self):
        for proc in self.processes:
            if not proc.is_alive():
                proc.join()

ProcessPool = processpool()