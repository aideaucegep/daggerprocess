import time
from dprocess import DaggerProcess, ProcessType
from status import Status
import asyncio
from messaging import Messaging, Message, StatusMessage
import os
from datetime import datetime
import numbers


class Process(DaggerProcess):
    process_type = ProcessType.ASYNC

    async def async_execute(self, *args):
        print("sleeping", args[0])
        if isinstance(args[0], numbers.Number):
            await asyncio.sleep(args[0])
        Messaging.post(StatusMessage(self.id, Status.DONE))
