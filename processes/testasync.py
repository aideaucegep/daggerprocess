import time
from dprocess import DaggerProcess, ProcessType
from status import Status
import asyncio
from messaging import Messaging, Message, StatusMessage

class Test(DaggerProcess):
    process_type = ProcessType.ASYNC

    async def async_execute(self, *args):
        Messaging.post(StatusMessage(self.id, Status.RUNNING))
        Messaging.post(Message(self.id, "some custom message about the process"))
        await asyncio.sleep(args[0])
        Messaging.post(StatusMessage(self.id, Status.DONE))
        self.status = Status.DONE

