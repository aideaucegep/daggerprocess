import time
from dprocess import DaggerProcess, ProcessType
from status import Status
import asyncio
from messaging import Messaging, Message, StatusMessage
import os
from datetime import datetime


class Process(DaggerProcess):
    process_type = ProcessType.ASYNC

    def test_files(self, filename, startdate):
        if os.path.exists(filename):
            if startdate:
                if startdate < datetime.utcfromtimestamp(os.path.getmtime(filename)):
                    return True
                else:
                    return False
            else:
                return True

    async def async_execute(self, *args):
        print("PROCESS RECEIVED:", args)
        if(len(args) >= 2):
            filename, startdate, *tail = args
            startdate = datetime.strptime(startdate, "%Y-%m-%d")
        else:
            filename = args[0]
            startdate = None

        found = False
        while True:
            if self.test_files(filename, startdate):
                if found:
                    Messaging.post(StatusMessage(self.id, Status.UPDATED))
                    startdate = datetime.utcfromtimestamp(
                        os.path.getmtime(filename))
                else:
                    Messaging.post(StatusMessage(self.id, Status.DONE))
                    startdate = datetime.utcfromtimestamp(
                        os.path.getmtime(filename))
                    found = True
            await asyncio.sleep(1)


if __name__ == "__main__":
    p = Process()
    p.execute(
        "//Mtldoc/secteurs/CVA Desk/Temp/{{tm1:yyyymmdd}}.testfile.csv", "{{today}}")
