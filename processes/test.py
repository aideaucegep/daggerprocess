import time
from dprocess import DaggerProcess, ProcessType
from status import Status
from messaging import Messaging, Message, StatusMessage


class Test(DaggerProcess):
    process_type = ProcessType.NORMAL

    def execute(self, *args):
        Messaging.post(StatusMessage(self.id, Status.RUNNING))
        time.sleep(args[0])
        Messaging.post(Message(self.id, "some custom message about the process"))
        time.sleep(args[0])
        Messaging.post(StatusMessage(self.id, Status.DONE))
        self.status = Status.DONE


