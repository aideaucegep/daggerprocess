import yaml
from dag import Dag
import asyncio
from messaging import Collector, Message, StatusMessage
from multiprocessing import Process
import multiprocessing as mp
from redis_socket_bridge import serve as SocketStart
from server import run as ServerStart
from processcollector import ProcessPool
from taskmanager import TaskManager
from timemanager import TimeManager
from status import Status
#!/usr/bin/env python
import signal
import sys
import websockets
import log
import jsons
import sys
jsons.suppress_warnings()


def signal_handler(sig, frame):
    log.info('KILLING: all processes')
    ProcessPool.killall()
    sys.exit(0)


class Manager():
    def __init__(self, dagfilepath: str):
        with open(dagfilepath, 'r') as yamlfile:
            data = yaml.load(yamlfile, Loader=yaml.FullLoader)
            self.dagDefinition = data
            self.dag = Dag(data["root"])
            Dag.dag = self.dag
            self.count = 0

    async def pulse_forever(self):
        while True:
            self.count += 1
            print('Pulse : ' + str(self.count), end='\r')
            self.dag.pulse()
            TaskManager.processMessages()
            TimeManager.run()
            await asyncio.sleep(0.3)

    async def run(self):
        self.spawnServer()
        asyncio.create_task(ServerStart())
        # asyncio.create_task(SocketStart())
        log.info("STARTING: http server")
        await asyncio.sleep(5)
        await self.pulse_forever()

    def spawnServer(self):
        p = Process(target=SocketStart, name="WebSocket")
        ProcessPool.add(p)
        p.start()


if __name__ == '__main__':
    if len(sys.argv) > 2 and sys.argv[1] == "test":
        import testnode
        asyncio.get_event_loop().run_until_complete(testnode.test(sys.argv[2]))
        pending = asyncio.Task.all_tasks()
        asyncio.get_event_loop().run_until_complete(asyncio.gather(*pending))
    else:
        log.info("info")
        log.debug("debug")
        log.critical("critical")
        log.error("error")
        mp.set_start_method("spawn")
        signal.signal(signal.SIGINT, signal_handler)
        manager = Manager("./mydag.yml")
        asyncio.run(manager.run())
        sys.exit(0)
