import datetime
from datetime import timedelta
from taskmanager import TaskManager
import workcalendar

class TimeManagerClass():
    def __init__(self):
        self.today = datetime.datetime.now().date()

    @property
    def tm1(self):
        return workcalendar.tm1(self.today)

    @property
    def tm2(self):
        return workcalendar.tm2(self.today)

    def tmx(self, days:int):
        return workcalendar.tmx(self.today, days)

    def run(self):
        if self.today and self.today != datetime.datetime.now().date():
            TaskManager.resetSubTree("root")
        self.today = datetime.datetime.now().date()


TimeManager = TimeManagerClass()