import log
from status import Status
import json
from aiohttp import web
import aiohttp_cors
import asyncio
import yaml
import jsons
from multiprocessing import Process
from taskmanager import TaskManager
from dag import Dag
routes = web.RouteTableDef()
import log

async def dag(request):
    return web.json_response(Dag.dag.toDictionary())


async def getProgress(request):
    return web.Response(text=jsons.dumps(TaskManager.state))


async def resetNode(request):
    data = await request.json()
    TaskManager.resetSubTree(pid=data["id"])
    return web.Response(text=jsons.dumps({"status": "ok"}))


async def run():
    app = web.Application()
    app.add_routes([
        web.get('/dag', dag),
        web.get('/progress', getProgress),
        web.post('/node/reset', resetNode)])

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)
    # To enable CORS processing for specific route you need to add
    # that route to the CORS configuration object and specify its
    # CORS options.
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, 'localhost', 1423)
    await site.start()


def serve():
    asyncio.get_event_loop().run_until_complete(run())
    asyncio.get_event_loop().run_forever()
    log.critical("server closed")


def spawn():
    p = Process(target=serve)
    p.start()


if __name__ == "__main__":
    spawn()
